<?php
ob_start();

include("header.php")
?>
<form class="form-horizontal" action="" method="POST">
<fieldset>

<!-- Form Name -->
<legend>Form Name</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">adi</label>  
  <div class="col-md-4">
  <input id="textinput" name="adi" type="text" placeholder="Adınızı giriniz..." class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="soyadınız">Soyadınız</label>  
  <div class="col-md-4">
  <input id="soyadınız" name="soyadi" type="text" placeholder="soyadınızı giriniz..." class="form-control input-md" required="">
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="universite">Üniversite</label>
  <div class="col-md-4">
    <select id="universite" name="universite" class="form-control">
      <option value="1">Fırat Universitesi</option>
      <option value="2">ODTÜ</option>
      <option value="2">BİLKENT</option>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">Email</label>  
  <div class="col-md-4">
  <input id="email" name="email" type="text" placeholder="Email giriniz" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="sifre">Şifre</label>
  <div class="col-md-4">
    <input id="sifre" name="sifre" type="password" placeholder="Şifre giriniz..." class="form-control input-md" required="">
    
  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="confirm">Şifre (tekrar)</label>
  <div class="col-md-4">
    <input id="confirm" name="confirm" type="password" placeholder="Şifre (Tekrar)" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="biyografi">Biyografi</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="biyografi" name="biyografi"></textarea>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton"></label>
  <div class="col-md-4">
    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Kaydet</button>
  </div>
</div>

</fieldset>
</form>

<?php
if(isset($_POST['email'])){
$adi=$_POST['adi'];
$soyadi=$_POST['soyadi'];
$email=$_POST['email'];
$sifre=$_POST['sifre'];
$biyografi=$_POST['biyografi'];
$universite=$_POST['universite'];

include("baglanti.php");
$query = $db->prepare("INSERT INTO uyeler SET
adi = ?,
soyadi = ?,
email = ?,
sifre = ?,
universite = ?,
biyografi = ?
");
$insert = $query->execute(array(
     $adi, $soyadi,$email,$sifre,$universite,$biyografi
));
if ( $insert ){
    header('Location: ../erzncan_odev/login.php');
}
}
?>


